﻿using System;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FortCode.Handlers
{
    public class BasicAuthHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        public BasicAuthHandler(IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder, 
            ISystemClock clock) 
            : base(options, logger, encoder, clock)
        { }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
            {
                return AuthenticateResult.Fail("Authorization header not found");
            }

            try
            {
                return await Task.Run(() =>
                {
                    var authValue = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                    var bytes = Convert.FromBase64String(authValue.Parameter);
                    var credentials = Encoding.UTF8.GetString(bytes).Split(":");

                    var userName = credentials[0];
                    var password = credentials[1];

                    // TODO look up user from database
                    if (userName == "John" && password == "myPass") // Sm9objpteVBhc3M=
                    {
                        var claims = new[] {new Claim(ClaimTypes.Name, userName)};
                        var identity = new ClaimsIdentity(claims, Scheme.Name);
                        var principal = new ClaimsPrincipal(identity);
                        var ticket = new AuthenticationTicket(principal, Scheme.Name);

                        return AuthenticateResult.Success(ticket);
                    }

                    return AuthenticateResult.Fail("Invalid username/password");
                });
            }
            catch
            {
                // TODO Implement proper exception handling and never silently catch or use generic exception types
                return AuthenticateResult.Fail("Failed to authenticate");
            }
        }
    }
}