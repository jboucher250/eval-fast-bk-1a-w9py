﻿using System;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Data
{
    public class FortDbContext : DbContext
    {
        public FortDbContext(DbContextOptions options) : base(options) { }

        public DbSet<City> Cities { get; set; }
        public DbSet<Bar> Bars { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException(nameof(modelBuilder));

            modelBuilder.Seed();
        }
    }
}
