﻿using System.Collections.Generic;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Data
{
    public static class SeedData
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().HasData(
                GetCities());

            modelBuilder.Entity<User>().HasData(
                GetUsers());
        }

        private static readonly List<City> Cities = new List<City>
        {
            new City {Id = 1, Name = "Bangor", Country = "USA"},
            new City {Id = 2, Name = "Augusta", Country = "USA"},
            new City {Id = 3, Name = "Paris", Country = "France"},
            new City {Id = 4, Name = "St Steven", Country = "Canada"},
            new City {Id = 5, Name = "Perth Andover", Country = "Canada"}
        };

        private static readonly List<User> Users = new List<User>
        {
            new User {Id = 1, Name = "Bob"},
            new User {Id = 2, Name = "John"},
            new User {Id = 3, Name = "Steve"},
            new User {Id = 4, Name = "Sam"}
        };

        public static List<City> GetCities()
        {
            return Cities;
        }

        public static List<User> GetUsers()
        {
            return Users;
        }
    }
}
