﻿using System.Collections.Generic;
using System.Linq;
using FortCode.Data;
using FortCode.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class BarController : ControllerBase
    {
        private readonly FortDbContext _context;

        public BarController(FortDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Add([FromBody] AddBarRequest request)
        {
            // TODO add validation for string lengths, that the city exists, etc
            _context.Bars.Add(new Bar
            {
                Name = request.BarName,
                FavoriteDrink = request.BarFavoriteDrink,
                City = _context.Cities.Single(x => x.Id == request.CityId)
            });

            _context.SaveChanges();

            return Ok();
        }

        [HttpDelete]
        public IActionResult Remove(int barId)
        {
            if (barId == 0)
            {
                ModelState.AddModelError("barId", "Bar ID is required");

                return BadRequest(ModelState);
            }

            var bar = _context.Bars.SingleOrDefault();

            if (bar == null)
            {
                return NotFound();
            }

            // TODO at least verify the Bar was created by this user before allowing delete

            _context.Bars.Remove(bar);

            _context.SaveChanges();

            return Ok();
        }

        [HttpGet]
        [Route("city")]
        [ProducesResponseType(typeof(IEnumerable<Bar>), 200)]
        public IActionResult GetByCityId(int cityId)
        {
            var userName = HttpContext.User.Identity.Name;

            if (cityId == 0)
            {
                ModelState.AddModelError("cityId", "City ID is required");

                return BadRequest(ModelState);
            }

            // TODO Add validation that the city id is valid rather than returning nothing silently
            return Ok(_context.Bars.Where(x => x.User.Name == userName && x.City.Id == cityId));
        }

        [HttpGet]
        [Route("shared")]
        [ProducesResponseType(typeof(IEnumerable<SharedBarResponse>), 200)]
        public IActionResult GetShared()
        {
            // TODO Get shared bars from database

            var userName = HttpContext.User.Identity.Name;
            var user = _context.Users.Include("Bars").SingleOrDefault(x => x.Name == userName);

            if (user == null)
            {
                return NotFound();
            }

            var barNames = user.Bars.Select(x => x.Name).ToList();

            // TODO If this were not a exercise it would be better to enhance the data model by
            // enforcing rules to prevent data integrity issues that would likely occur with 
            // this quick design where cities aren't a managed list of reference/seeded data
            // and users can create bars as they feel fit that may ultimately be duplicates with slight name variations
            var sharedBars = _context.Bars.Where(x => barNames.Contains(x.Name) && x.User != user)
                .GroupBy(x => x.Name).ToList();

            return Ok(sharedBars.Select(x => new SharedBarResponse
            {
                BarName = x.Key,
                Count = x.Count()
            }));
        }
    }
}
