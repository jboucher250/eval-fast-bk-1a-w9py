﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using FortCode.Data;
using FortCode.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class CityController : ControllerBase
    {
        // TODO look up cities from database
        private static readonly string[] CityNames = {
            "Augusta", "Caribou", "Portland", "Bangor"
        };


        private readonly FortDbContext _context;

        public CityController(FortDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Add([FromBody] AddCityRequest request)
        {
            // TODO add validation for string lengths, that the city doesn't exist, etc
            _context.Cities.Add(new City
            {
                Name = request.CityName,
                Country = request.CityCountry
            });

            _context.SaveChanges();

            return Ok();
        }

        [HttpDelete]
        public IActionResult Remove(int cityId)
        {
            if (cityId == 0)
            {
                ModelState.AddModelError("cityId", "City ID is required");

                return BadRequest(ModelState);
            }

            var city = _context.Cities.Include("Bars")
                .SingleOrDefault();

            if (city == null)
            {
                return NotFound();
            }

            // TODO at least verify the City was created by this user before allowing delete. This is another
            // example where it would be better suited in a real-life scenario to have a more robust/managed data model

            var bars = city.Bars;

            foreach (var bar in bars)
            {
                _context.Bars.Remove(bar);
            }

            _context.Cities.Remove(city);

            _context.SaveChanges();

            return Ok();
        }

        [HttpGet]
        [Route("list")]
        [ProducesResponseType(typeof(IEnumerable<City>), 200)]
        public IActionResult GetList()
        {
            // TODO The aforementioned updates to the data model are necessary to properly implement this retrieval from
            // the database. I have run out of time for this weekend but essentially it would be best to have an intermediate
            // table to relate users -> cities and have users Add to that table rather than to have them add Cities directly

            return Ok(CityNames.Select((city, index) => new City
                {
                    Name = CityNames[index],
                    Country = "USA"
                })
                .ToArray());
        }
    }
}
