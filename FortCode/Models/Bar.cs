﻿using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;

namespace FortCode.Models
{
    public class Bar
    {
        public int Id { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [MaxLength(100)]
        [Required]
        public string FavoriteDrink { get; set; }

        [Required]
        public City City { get; set; }

        [Required]
        public User User { get; set; }
    }
}