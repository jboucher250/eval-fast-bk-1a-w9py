﻿using System.ComponentModel.DataAnnotations;

namespace FortCode.Models
{
    public class AddBarRequest
    {
        [Required]
        public string BarName { get; set; }

        [Required]
        public string BarFavoriteDrink { get; set; }

        [Required]
        public int CityId { get; set; }
    }
}