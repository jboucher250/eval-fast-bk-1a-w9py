﻿using System.ComponentModel.DataAnnotations;

namespace FortCode.Models
{
    public class AddCityRequest
    {
        [Required]
        public string CityName { get; set; }

        [Required]
        public string CityCountry { get; set; }
    }
}