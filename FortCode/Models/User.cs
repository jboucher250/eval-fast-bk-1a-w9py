﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Models
{
    public class User
    {
        public int Id { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        public IEnumerable<Bar> Bars { get; set; }
    }
}