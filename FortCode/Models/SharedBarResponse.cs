﻿namespace FortCode.Models
{
    public class SharedBarResponse
    {
        public int Count { get; set; }

        public string BarName { get; set; }
    }
}