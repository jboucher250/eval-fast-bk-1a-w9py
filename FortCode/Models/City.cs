﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FortCode.Models
{
    public class City
    {
        public int Id { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [MaxLength(100)]
        [Required]
        public string Country { get; set; }

        public IEnumerable<Bar> Bars { get; set; }
    }
}