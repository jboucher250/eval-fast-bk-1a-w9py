# NOTES

I have run out of time for this weekend and have not had a chance to run it since the first commit (stubbed controllers). 
However, I have tried to mark up the code with TODOs. I believe the combination of what I have completed thus far combined with the TODOs noting next steps will give an accurate assessment.

## USAGE

Upon completion, the design would be as follows:

* Create a base64 encoded string with a tool such as https://www.base64encode.org/ of username:password
* Using a tool such as Postman, include in your request header Authorization:    Basic <Base64String>
* Call operations as necessary on /api/city and /api/bar routes. Appologizes for insufficient time to give more robust information on those APIs, but hopefully you will find them self-explanatory upon opening the Controllers.
* Basic Auth would not be something to keep long-term and was simply for demonstration

Please see TODOs in code for more details on what would need to be completed if this were to go to production. Unit tests would need to be completed as well

Application is also not completely layered. Future TODO in addition to what is documented in the code would be to add more layering such as a data layer to isolate the logic used on the DB context out from the Controllers.